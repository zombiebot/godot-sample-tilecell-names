extends Node2D

var tile_map
var tile_set
var empty_cell_index = -1
var walkable_color = Color(255.0, 255.0, 255.0)

func _ready():
	tile_map = get_parent().get_node("TileMap")
	tile_set = tile_map.tile_set

func _draw():
	var even = true
	for y in range(50):
		for x in range(50):
			var pos = Vector2(20 + y * 50, 20 + x * 50)
			if is_cell_walkable(pos):
				draw_rect(Rect2(pos, Vector2(10, 10)), walkable_color, false)

func is_cell_walkable(world_pos):
	var tile_pos = tile_map.world_to_map(world_pos)
	var cell = tile_map.get_cellv(tile_pos)
	if cell == empty_cell_index:
		return false
	var cell_name = tile_set.tile_get_name(cell)
	if cell_name == "lane" or cell_name == "field":
		return true
	return false